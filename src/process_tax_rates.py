#!/usr/bin/python3

import os
import csv
import mysql.connector

from dotenv import load_dotenv
from decimal import Decimal

#===============================================================================================================
# GLOBALS
#===============================================================================================================
tax_rates_csv = os.getcwd() + "/data/tax_rates.csv"
tax_dict = {
    "municipality_id": "",
    "date_used": "",
    "canton_tax_rate": "",
    "municipality_tax_rate": "",
    "church_ref_tax_rate": "",
    "church_chr_cath_tax_rate": "",
    "church_rom_cath_tax_rate": "",
}
processed_bfs_ids = []

#===============================================================================================================
# INIT
#===============================================================================================================
load_dotenv(dotenv_path=".env")

connection = mysql.connector.connect(
    user=os.getenv("ASA-USERNAME"), 
    password=os.getenv("ASA-PASSWORD"), 
    database=os.getenv("ASA-DATABASE"), 
    host=os.getenv("ASA-HOST"), 
    port=os.getenv("ASA-PORT")
)
cursor = connection.cursor()

# Preparation for data insertion into db
placeholders = ', '.join(['%s'] * len(tax_dict))
columns = ', '.join(tax_dict.keys())
add_tax = (
    "INSERT INTO tax (%s) VALUES (%s)" % (columns, placeholders)
)

#===============================================================================================================
# FUNCTIONS
#===============================================================================================================
# Fetch municipality id by bfs_id
def fetch_municipality_id_by_bfs_id(bfs_id):
    try:
        cursor.execute("SELECT id FROM municipality WHERE bfs_id = %s", (bfs_id,))
        id = cursor.fetchone()[0]
        return id
    except:
        print("Could not find a municipality with bfs_id: " + bfs_id)
        return

# Processes all municipalities (Gemeinde)
def process_tax():
    with open(tax_rates_csv, encoding="utf-8") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=",")
        next(csv_reader, None) # skip first line (header)

        print("Writing tax rates to database...")
        for item in csv_reader:
            id = fetch_municipality_id_by_bfs_id(item[2])
            if id == None:
                continue
            else:
                tax_dict["municipality_id"] = id
                tax_dict["date_used"] = "2022-01-01"
                tax_dict["tax_burden"] = Decimal(item[8])
                tax_dict["canton_tax_rate"] = Decimal(item[3])
                tax_dict["municipality_tax_rate"] = Decimal(item[4])
                tax_dict["church_ref_tax_rate"] = Decimal(item[5])
                tax_dict["church_chr_cath_tax_rate"] = Decimal(item[7])
                tax_dict["church_rom_cath_tax_rate"] = Decimal(item[6])
                cursor.execute(add_tax, list(tax_dict.values()))
                processed_bfs_ids.append(item[2])

    connection.commit()
    print("All tax rates written to database...")

process_tax()

print("Terminating script...")
cursor.close()
connection.close()
