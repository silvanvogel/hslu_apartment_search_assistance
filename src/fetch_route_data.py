#!/usr/bin/python3

from cmath import e
import os
import requests
import json
import mysql.connector

from dotenv import load_dotenv
from urllib.parse import quote_plus
from enum import Enum

#===============================================================================================================
# GLOBALS
#===============================================================================================================
class Mode(Enum):
    DRIVING = 1
    TRANSIT = 2
    WALKING = 3

length_dict = {
    "mode_id": "",
    "apartment_id": "",
    "distance": "",
    "duration": ""
}

#===============================================================================================================
# INIT
#===============================================================================================================
load_dotenv(dotenv_path=".env")

connection = mysql.connector.connect(
    user=os.getenv("ASA-USERNAME"), 
    password=os.getenv("ASA-PASSWORD"), 
    database=os.getenv("ASA-DATABASE"), 
    host=os.getenv("ASA-HOST"), 
    port=os.getenv("ASA-PORT")
)
cursor = connection.cursor()

# Preparation for length data insertion into db
placeholders_length = ', '.join(['%s'] * len(length_dict))
columns_length = ', '.join(length_dict.keys())
add_length = (
    "INSERT INTO length (%s) VALUES (%s)" % (columns_length, placeholders_length)
)

#===============================================================================================================
# HELPER FUNCTIONS
#===============================================================================================================
# Write modes (driving, transit, walking) to db
def process_modes():
    cursor.execute("SELECT COUNT(1) FROM mode WHERE id = 1")
    mode_count = cursor.fetchone()[0]

    if mode_count > 0:
        print("Modes already written to database")
    else:
        for i in Mode:
            cursor.execute("INSERT INTO mode (name) VALUES (%s)", (i.name.lower(),))
        connection.commit()
        print("Modes written to database")

# Fetch all apartments (id and public_address) from db
def fetch_apartments():
    try:
        cursor.execute("SELECT id, public_address FROM apartment")
        apartments = cursor.fetchall()
        return apartments
    except:
        return

#===============================================================================================================
# FUNCTIONS
#===============================================================================================================
# Fetch route data from google api
# @param mode: enum
def fetch_route_from_google_api(mode):
    print("Processing "+ mode.name.lower() +" data from Google Distance Matrix API...")
    uri = "https://maps.googleapis.com/maps/api/distancematrix/json?"
    destinations = "&destinations=Hochschule%20Luzern%20%E2%80%93%20Informatik%2C%20Suurstoffi%2C%20Rotkreuz%20ZG"
    transportation_mode = "&mode=" + mode.name.lower()
    units = "&units=metric"
    api_key = "&key=" + os.getenv("ASA-G-API-KEY")
    headers = {
        "Accept-Language": "de-CH"
    }

    apartments = fetch_apartments()
    for apartment in apartments:
        length_dict["mode_id"] = mode.value
        length_dict["apartment_id"] = apartment[0]

        # Prepare API call
        origins = "origins=" + quote_plus(apartment[1])
        url = uri + origins + destinations + transportation_mode + units + api_key

        # Fetch distance from api
        response = requests.request("GET", url,headers=headers)
        response_data = json.loads(response.text)
        
        # save distance in meters and duration in seconds
        try:
            length_dict["distance"] = response_data["rows"][0]["elements"][0]["distance"]["value"]
            length_dict["duration"] = response_data["rows"][0]["elements"][0]["duration"]["value"]
            cursor.execute(add_length, list(length_dict.values()))
        except:
            print("The " + mode.name.lower() + " route to " + apartment[1] + " could not be calculated.")
            pass

    connection.commit()
    print("All " + mode.name.lower() + " routes written to database...")

process_modes()
fetch_route_from_google_api(Mode.DRIVING)
fetch_route_from_google_api(Mode.TRANSIT)
fetch_route_from_google_api(Mode.WALKING)

cursor.close()
connection.close()
