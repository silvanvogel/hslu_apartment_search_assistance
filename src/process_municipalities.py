#!/usr/bin/python3

import os
import csv
import mysql.connector

from dotenv import load_dotenv

#===============================================================================================================
# GLOBALS
#===============================================================================================================
municipalities_csv = os.getcwd() + "/data/municipalities.csv"
municipality_dict = {
    "canton_id": "",
    "name": "",
    "bfs_id": "",
}

zip_codes_csv = os.getcwd() + "/data/post_zip_codes.csv"
zip_code_dict = {
    "municipality_id": "",
    "zip_code": "",
}
processed_zip_codes = []

#===============================================================================================================
# INIT
#===============================================================================================================
load_dotenv(dotenv_path=".env")

connection = mysql.connector.connect(
    user=os.getenv("ASA-USERNAME"), 
    password=os.getenv("ASA-PASSWORD"), 
    database=os.getenv("ASA-DATABASE"), 
    host=os.getenv("ASA-HOST"), 
    port=os.getenv("ASA-PORT")
)
cursor = connection.cursor()

# Preparation for municipality data insertion into db
placeholders_municipality = ', '.join(['%s'] * len(municipality_dict))
columns_municipality = ', '.join(municipality_dict.keys())
add_municipality = (
    "INSERT INTO municipality (%s) VALUES (%s)" % (columns_municipality, placeholders_municipality)
)

# Preparation for zip_code data insertion into db
placeholders_zip_code = ', '.join(['%s'] * len(zip_code_dict))
columns_zip_code = ', '.join(zip_code_dict.keys())
add_zip_code = (
    "INSERT INTO zip_code (%s) VALUES (%s)" % (columns_zip_code, placeholders_zip_code)
)

#===============================================================================================================
# HELPER FUNCTIONS
#===============================================================================================================
# Fetch canton_id by abbreviation
def fetch_canton_id_by_abbreviation(abbreviation):
    cursor.execute("SELECT id FROM canton WHERE abbreviation = %s", (abbreviation,))
    return cursor.fetchone()[0]

# Fetch municipality_id by bfs_id
def fetch_municipality_id_by_bfs_id(bfs_id):
    cursor.execute("SELECT id FROM municipality WHERE bfs_id = %s", (bfs_id,))
    return cursor.fetchone()[0]

#===============================================================================================================
# FUNCTIONS
#===============================================================================================================
# Processes all municipalities (Gemeinde)
def process_municipalities():
    with open(municipalities_csv, encoding="utf-8") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=",")
        next(csv_reader, None) # skip first line (header)

        print("Writing municipalities to database...")
        for item in csv_reader:
            municipality_dict["canton_id"] = fetch_canton_id_by_abbreviation(item[0])
            municipality_dict["name"] = item[3]
            municipality_dict["bfs_id"] = item[2]
            cursor.execute(add_municipality, list(municipality_dict.values()))

    connection.commit()
    print("All municipalities written to database...")

# Read bfs_id - zip_code connections from csv
def process_zip_codes():
    with open(zip_codes_csv, encoding="utf-8") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=";")
        next(csv_reader, None) # skip first line (header)

        print("Writing zip codes to database...")
        for item in csv_reader:
            # 6806 is the last Swiss bfs_id; > 7000 is Liechtenstein
            if item[4] not in processed_zip_codes and int(item[2]) < 7000:
                zip_code_dict["municipality_id"] = fetch_municipality_id_by_bfs_id(item[2])
                zip_code_dict["zip_code"] = item[4]
                cursor.execute(add_zip_code, list(zip_code_dict.values()))
                processed_zip_codes.append(item[4])

    connection.commit()
    print("All zip codes written to database...")

process_municipalities()
process_zip_codes()

print("Terminating script...")
cursor.close()
connection.close()
