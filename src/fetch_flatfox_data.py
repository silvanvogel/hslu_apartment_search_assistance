#!/usr/bin/python3

import os
import requests
import json
import mysql.connector

from dotenv import load_dotenv
from requests_html import HTMLSession

#===============================================================================================================
# GLOBALS
#===============================================================================================================
apartment_dict = {
    "municipality_id": "",
    "pk": "",
    "object_category": "",
    "rent": "",
    "public_title": "",
    "description_title": "",
    "description": "",
    "space": "",
    "number_of_rooms": "",
    "floors": "",
    "street": "",
    "city": "",
    "public_address": "",
    "year_built": "",
    "year_renovated": "",
    "moving_date": "",
    "is_furnished": "",
    "is_temporary": "",
}

#===============================================================================================================
# INIT
#===============================================================================================================
load_dotenv(dotenv_path=".env")

connection = mysql.connector.connect(
    user=os.getenv("ASA-USERNAME"), 
    password=os.getenv("ASA-PASSWORD"), 
    database=os.getenv("ASA-DATABASE"), 
    host=os.getenv("ASA-HOST"), 
    port=os.getenv("ASA-PORT")
)
cursor = connection.cursor(buffered=True)

# Preparation for data insertion into db
placeholders = ', '.join(['%s'] * len(apartment_dict))
columns = ', '.join(apartment_dict.keys())
add_apartment = (
    "INSERT INTO apartment (%s) VALUES (%s)" % (columns, placeholders)
)

#===============================================================================================================
# FUNCTIONS
#===============================================================================================================
# Fetch municipality id by zip_code
def fetch_municipality_id_by_zip_code(zip_code):
    try:
        cursor.execute("SELECT municipality_id FROM zip_code WHERE zip_code = %s", (zip_code,))
        municipality_id = cursor.fetchone()[0]
        return municipality_id
    except:
        print("Could not find a municipality with zip_code: " + zip_code)
        return

# Fetch data from flatfox API and insert into database
def process_apartments(data):
    apartment_counter = 0
    for apartment in data["results"]:
        municipality_id = fetch_municipality_id_by_zip_code(apartment["zipcode"])

        if (apartment["object_category"] == "APARTMENT" and apartment["offer_type"] == "RENT" and 
            int(apartment["zipcode"]) < 9485 and int(apartment["zipcode"]) > 9498 and municipality_id != None and apartment["price_display"] != None):
            apartment_dict["municipality_id"] = municipality_id
            apartment_dict["pk"] = apartment["pk"]
            apartment_dict["object_category"] = apartment["object_category"]
            apartment_dict["rent"] = apartment["price_display"]
            apartment_dict["public_title"] = apartment["public_title"]
            apartment_dict["description_title"] = apartment["description_title"]
            apartment_dict["description"] = apartment["description"]
            apartment_dict["space"] = apartment["space_display"]
            apartment_dict["number_of_rooms"] = apartment["number_of_rooms"]
            apartment_dict["floors"] = apartment["floor"]
            apartment_dict["street"] = apartment["street"]
            apartment_dict["city"] = apartment["city"]
            apartment_dict["public_address"] = apartment["public_address"]
            apartment_dict["year_built"] = apartment["year_built"]
            apartment_dict["year_renovated"] = apartment["year_renovated"]
            apartment_dict["moving_date"] = apartment["moving_date"]
            apartment_dict["is_furnished"] = apartment["is_furnished"]
            apartment_dict["is_temporary"] = apartment["is_temporary"]
            cursor.execute(add_apartment, list(apartment_dict.values()))
            apartment_counter += 1
        else:
            continue
    
    connection.commit()
    return apartment_counter

# Flatfox uses pagination (security against DOS attacks); Paginate through the results by raising offset by 100
def fetch_apartments():
    session = HTMLSession()
    offset = 0
    apartment_counter = 0

    while True:
        response = session.get("https://flatfox.ch/api/v1/public-listing/?limit=100&offset=" + str(offset))
        data = json.loads(response.text)
        if not data["results"]:
            break
        apartment_counter += process_apartments(data)
        offset += 100
        if not session.next():
            break

    session.close()
    print(apartment_counter + " apartments fetched and written to db")

print("Fetching apartments and writing to database...")
fetch_apartments()

print("Terminating script...")
cursor.close()
connection.close()

