#!/usr/bin/python3

import os
import csv
import mysql.connector

from dotenv import load_dotenv

#===============================================================================================================
# GLOBALS
#===============================================================================================================
cantons_csv = os.getcwd() + "/data/cantons.csv"
canton_dict = {
    "name": "",
    "abbreviation": "",
}

#===============================================================================================================
# INIT
#===============================================================================================================
load_dotenv(dotenv_path=".env")

connection = mysql.connector.connect(
    user=os.getenv("ASA-USERNAME"), 
    password=os.getenv("ASA-PASSWORD"), 
    database=os.getenv("ASA-DATABASE"), 
    host=os.getenv("ASA-HOST"), 
    port=os.getenv("ASA-PORT")
)
cursor = connection.cursor()

# Preparation for data insertion into db
placeholders = ', '.join(['%s'] * len(canton_dict))
columns = ', '.join(canton_dict.keys())
add_canton = (
    "INSERT INTO canton (%s) VALUES (%s)" % (columns, placeholders)
)

#===============================================================================================================
# FUNCTIONS
#===============================================================================================================
# Processes all cantons
def process_cantons():
    with open(cantons_csv, encoding="utf-8") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=",")
        next(csv_reader, None) # skip first line (header)

        print("Writing cantons to database...")
        for item in csv_reader:
            canton_dict["abbreviation"] = item[1]
            canton_dict["name"] = item[2]
            cursor.execute(add_canton, list(canton_dict.values()))

    connection.commit()
    print("All cantons written to database...")


process_cantons()

print("Terminating script...")
cursor.close()
connection.close()
