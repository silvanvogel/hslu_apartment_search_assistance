USE test_decision_assistance;

CREATE VIEW all_apartment AS SELECT * FROM apartment;

CREATE VIEW apartment_score AS
SELECT apartment.id, apartment.rent, tax.canton_tax_rate, tax.municipality_tax_rate, length.distance, length.duration, mode.name,
((1 - ((apartment.rent - 645) / (3000 - 645))) * 0.9 + 0.1) as apartment_score_rent,
(1 - ((length.duration - 437) / (24461 - 437))) as apartment_score_duration,
(((1 - ((apartment.rent - 645) / (3000 - 645))) * 0.9 + 0.1) * (0.1 + ((1 - ((length.duration - 437) / (24461 - 437))) - 0.8) * (1 - 0.1)/(1 - 0.8)) * 10) as apartment_score
FROM apartment
INNER JOIN tax ON apartment.municipality_id = tax.id
INNER JOIN length ON apartment.id = length.apartment_id
INNER JOIN mode ON length.mode_id = mode.id
WHERE apartment.id = 150;

