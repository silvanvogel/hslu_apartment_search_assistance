DROP DATABASE IF EXISTS decision_assistance;
CREATE DATABASE IF NOT EXISTS decision_assistance;
USE decision_assistance;

CREATE TABLE IF NOT EXISTS canton (
  id integer NOT NULL UNIQUE AUTO_INCREMENT,
  name varchar(300) NOT NULL,
  abbreviation char(5) NOT NULL,
  PRIMARY KEY (id)
) CHARSET utf8mb4 ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS municipality (
  id integer NOT NULL UNIQUE AUTO_INCREMENT,
  canton_id integer NOT NULL,
  name varchar(300) NOT NULL,
  bfs_id decimal(6) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (canton_id) REFERENCES canton(id)
) CHARSET utf8mb4 ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS zip_code (
  id integer NOT NULL UNIQUE AUTO_INCREMENT,
  municipality_id integer NOT NULL,
  zip_code decimal(6),
  PRIMARY KEY (id),
  FOREIGN KEY (municipality_id) REFERENCES municipality(id)
) CHARSET utf8mb4 ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS tax (
  id integer NOT NULL UNIQUE AUTO_INCREMENT,
  municipality_id integer NOT NULL,
  date_used date NOT NULL,
  tax_burden decimal(8,4) NOT NULL,
  canton_tax_rate decimal(8,4) NOT NULL,
  municipality_tax_rate decimal(8,4) NOT NULL,
  church_ref_tax_rate decimal(8,4) NOT NULL,
  church_chr_cath_tax_rate decimal(8,4) NOT NULL,
  church_rom_cath_tax_rate decimal(8,4) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (municipality_id) REFERENCES municipality(id)
) CHARSET utf8mb4 ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS apartment (
  id integer NOT NULL UNIQUE AUTO_INCREMENT,
  municipality_id integer NOT NULL,
  pk integer NOT NULL,
  rent decimal(15,4) NOT NULL,
  public_title varchar(400),
  description_title varchar(400),
  description varchar(9000),
  space integer,
  number_of_rooms tinyint,
  floors tinyint,
  street varchar(300),
  city varchar(300),
  public_address varchar(650),
  object_category varchar(20) NOT NULL,
  year_built varchar(10),
  year_renovated varchar(10),
  moving_date date,
  is_furnished boolean,
  is_temporary boolean,
  PRIMARY KEY (id),
  FOREIGN KEY (municipality_id) REFERENCES municipality(id)
) CHARSET utf8mb4 ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS mode (
  id integer NOT NULL UNIQUE AUTO_INCREMENT,
  name varchar(40) NOT NULL,
  PRIMARY KEY (id)
) CHARSET utf8mb4 ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS length (
  id integer NOT NULL UNIQUE AUTO_INCREMENT,
  mode_id integer NOT NULL,
  apartment_id integer NOT NULL,
  distance integer NOT NULL,
  duration integer NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (mode_id) REFERENCES mode(id),
  FOREIGN KEY (apartment_id) REFERENCES apartment(id)
) CHARSET utf8mb4 ENGINE=InnoDB;
