-- Installing validate_password component
INSTALL COMPONENT 'file://component_validate_password';

SET GLOBAL validate_password.check_user_name = ON;
SET GLOBAL validate_password.dictionary_file = "C:\\ProgramData\\MySQL\\MySQL Server 8.0\\rockyou.txt";
SET GLOBAL validate_password.length = 10;
SET GLOBAL validate_password.mixed_case_count = 1;
SET GLOBAL validate_password.number_count = 1;
SET GLOBAL validate_password.special_char_count = 1;
-- 2 = STRONG; Tests performed: Length; numeric, lowercase/uppercase and special characters; dictionary file
SET GLOBAL validate_password.policy = 2;

SHOW VARIABLES LIKE 'validate_password.%';