/*#######################################################################################
 USER CREATION
#######################################################################################*/
/*
Security by:
 - using strong passwords
 - using views for data access
 - obscurity (using not standard usernames)
*/

-- create REMOTE user: hasa_david@'%'
DROP USER IF EXISTS 'sample_user'@'%';
CREATE USER IF NOT EXISTS 'sample_user'@'%' IDENTIFIED BY 'sample_data';
SELECT VALIDATE_PASSWORD_STRENGTH('sample_data'); -- PW strength: 100/100

-- create REMOTE user: hasa_lenny@'%'
DROP USER IF EXISTS 'sample_user'@'%';
CREATE USER IF NOT EXISTS 'sample_user'@'%' IDENTIFIED BY 'sample_data';
SELECT VALIDATE_PASSWORD_STRENGTH('sample_data'); -- PW strength: 100/100

-- create REMOTE user: hasa_mimi@'%'
DROP USER IF EXISTS 'sample_user'@'%';
CREATE USER IF NOT EXISTS 'sample_user'@'%' IDENTIFIED BY 'sample_data';
SELECT VALIDATE_PASSWORD_STRENGTH('sample_data'); -- PW strength: 100/100

-- create REMOTE user: hasa_silvan@'%'
DROP USER IF EXISTS 'sample_user'@'%';
CREATE USER IF NOT EXISTS 'sample_user'@'%' IDENTIFIED BY 'sample_data';
SELECT VALIDATE_PASSWORD_STRENGTH('sample_data'); -- PW strength: 100/100

-- create REMOTE user: hasa_public@'%'
DROP USER IF EXISTS 'sample_user'@'%';
CREATE USER IF NOT EXISTS 'sample_user'@'%' IDENTIFIED BY 'sample_data';
SELECT VALIDATE_PASSWORD_STRENGTH('sample_data'); -- PW strength: 100/100

-- create LOCAL user: hasa_local_admin@localhost
DROP USER IF EXISTS 'sample_user'@'localhost';
CREATE USER IF NOT EXISTS 'sample_user'@'localhost' IDENTIFIED BY 'sample_data';
SELECT VALIDATE_PASSWORD_STRENGTH('sample_data'); -- PW strength: 100/100

/*#######################################################################################
 USER PRIVILEGES
#######################################################################################*/
-- create user ROLES
DROP ROLE IF EXISTS 'sample_role1', 'sample_role2';
CREATE ROLE IF NOT EXISTS 'sample_role1', 'sample_role2';

-- create sample_role1 PRIVILEGES
GRANT ALL ON test_decision_assistance.* TO 'sample_role1';
GRANT SELECT ON test_decision_assistance.* TO 'sample_role2';

GRANT ALL ON decision_assistance.* TO 'sample_role1';
GRANT SELECT ON decision_assistance.* TO 'sample_role2';

-- GRANT PRIVILEGES
GRANT 'sample_role1' TO 'sample_user'@'%', 'sample_user'@'%', 'sample_user'@'%', 'sample_user'@'%', 'sample_user'@'localhost';
GRANT 'sample_role2' TO 'sample_user'@'%';

SHOW GRANTS FOR 'sample_user'@'localhost';
SHOW GRANTS FOR 'sample_user'@'%';

-- ASSIGN PRIVILEGES
SET DEFAULT ROLE ALL TO 'sample_user'@'%', 'sample_user'@'%', 'sample_user'@'%', 'sample_user'@'%', 'sample_user'@'%', 'sample_user'@'localhost';
FLUSH PRIVILEGES;