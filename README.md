# Apartment Search Assistance for HSLU Students
A website providing useful information for hslu students on appartment search.
- Appartment adverts are fetched from the flatfox API (https://flatfox.ch/en/docs/api/#/)
- Tax data is fetched from admin.ch (https://swisstaxcalculator.estv.admin.ch/)

## Setup
After cloning the repository a few files need to be untracked. This can be achieved by executing the following command.

```
git update-index --assume-unchanged .env data/flatfox_data.json data/tax.json
```

> Please do not push any files before executing the above command.

## Requirements
For `fetch_flatfox_data.py` and `fetch_route_data.py`:
- Install `requests` through pip

For database access:
- Install `mysql-connector` through pip
- Install `python-dotenv` through pip

### Example - Windows
`$ python -m pip install requests`

### Example - Linux
`$ pip install requests`

## API data points

### Apartment - [flatfox.ch](https://flatfox.ch/en/docs/api/#/)

| Attributes (API)     | Attributes (MySQL) | Note                                               |
|----------------------|--------------------|----------------------------------------------------|
| pk                   | PK                 | 19382                                              |
| url                  | ?                  | "/en/flat/kreuzfeldstrasse-55-4932-lotzwil/23239/" |
| object_category      | ?                  | "SHARED" / "APARTMENT"                             |
| object_type          | ?                  | "SHARED" / "APARTMENT"                             |
| price_display        | Price              | 2200                                               |
| price_display_type   | ?                  | "TOTAL"                                            |
| price_unit           | ?                  | "monthly"                                          |
| rent_net             | ?                  | null                                               |
| rent_charges         | ?                  | 175                                                |
| rent_gross           | ?                  | 2200                                               |
| short_title          | ?                  | "2 ½ rooms apartment"                              |
| public_title         | PublicTitle        | "Uetlibergstrasse 129 - CHF 2’200 per month"       |
| rent_title           | ?                  | "Rent a 2 ½ rooms apartment in Zürich"             |
| description_title    | DescriptionTitle   | null                                               |
| description          | Description        | ""                                                 |
| space_display        | Space              | 75 - was noted as `livingspace`                    |
| number_of_rooms      | NumberOfRooms      | "2.5"                                              |
| floor                | Floor              | 1                                                  |
| is_furnished         | ?                  | false                                              |
| street               | Street             | "Uetlibergstrasse 129"                             |
| zipcode              | ZIPCode            | "8045"                                             |
| city                 | City               | "Zürich"                                           |
| public_address       | PublicAddress      | "Uetlibergstrasse 129, 8045 Zürich"                |
| year_built           | ?                  | null                                               |
| year_renovated       | ?                  | null                                               |
| moving_date          | ?                  | "2020-06-11"                                       |
| cover_image          | ?                  | only Id provided - would have to be scraped        |
| **distance_to_hslu** | DistanceToHSLU     | calculated by Google Distance Matrix API           |
| **duration_to_hslu** | DurationToHSLU     | calculated by Google Distance Matrix API           |

### Distance and duration - [Google Distance Matrix API](https://developers.google.com/maps/documentation/distance-matrix/distance-matrix)

#### Parameters

##### destination_address
One or more locations to use as the finishing point for calculating travel distance and time. The options for the destinations parameter are the same as for the origins parameter.

##### origin_addresses
The starting point for calculating travel distance and time. You can supply one or more locations separated by the pipe character (|), in the form of a place ID, an address, or latitude/longitude coordinates.

##### mode
For the calculation of distances and directions, you may specify the transportation mode to use. By default, DRIVING mode is used. By default, directions are calculated as driving directions. The following travel modes are supported:

- `driving` (default) indicates standard driving directions or distance using the road network.
- `walking` requests walking directions or distance via pedestrian paths & sidewalks (where available).
- `bicycling` requests bicycling directions or distance via bicycle paths & preferred streets (where available).
- `transit` requests directions or distance via public transit routes (where available). If you set the mode to transit, you can optionally specify either a `departure_time` or an `arrival_time`. If neither time is specified, the `departure_time` defaults to now (that is, the departure time defaults to the current time). You can also optionally include a `transit_mode` and/or a `transit_routing_preference`.

##### transit_mode
Specifies one or more preferred modes of transit. This parameter may only be specified for transit directions. The parameter supports the following arguments:

- `bus` indicates that the calculated route should prefer travel by bus.
- `subway` indicates that the calculated route should prefer travel by subway.
- `train` indicates that the calculated route should prefer travel by train.
- `tram` indicates that the calculated route should prefer travel by tram and light rail.
- `rail` indicates that the calculated route should prefer travel by train, tram, light rail, and subway. This is equivalent to `transit_mode=train|tram|subway`.

#### JSON Example
```
{
  "destination_addresses":
    [Hochschule Luzern – Informatik, Suurstoffi, Rotkreuz ZG"],
  "origin_addresses":
    ["Kreuzfeldstrasse 2, 4932 Lotzwil"],
  "rows":
    [
      {
        "elements":
          [
            {
              "distance": { "text": "69 km", "value": 69599 },
              "duration": { "text": "54 Minuten", "value": 3258 },
              "status": "OK",
            },
          ],
      },
    ],
  "status": "OK",
}
```

### Tax
| Attributes (XLS)     | Attributes (MySQL)   | Note                                               |
|----------------------|----------------------|----------------------------------------------------|
| BfS-Id               | BfSId                | used to create the connection to municipality      |
|                      | Year                 | currently only 2022                                |
| Kanton               | CantonTaxRate        | -                                                  |
| Gemeinde             | MunicipalityTaxRate  | -                                                  |
| Kirche ref.          | ChurchRefTaxRate     | -                                                  |
| Kirche röm.-kath.    | ChurchRomCathTaxRate | -                                                  |
| Kirche christkath.   | ChurchChrCathTaxRate | -                                                  |

## Data Import [WIP]
To import the data the MySQL Connector/Python is used, which enables Python programs to access MySQL databases, using an API that is compliant with the Python Database API Specification v2.0 (PEP 249). 

In order to keep the credentials for the database access secure, a `.env` file is used.
This will enable us to use environment variables for local development without polluting the global environment namespace. 
It will also keep the environment variable names and values isolated to the same project that utilizes them.

> Be sure to fill in the database credentials when trying to connect to the database

## Implementation

### Tax calculation - [admin.ch](https://swisstaxcalculator.estv.admin.ch/)
The tax calculation is made using the taxable income (**steuerbares Einkommen**). This is because the net income (Nettoeinkommen) minus deductions (Abzüge) - which can vary - would inevitably lead to calculation errors. Thus, the responsibility for any deductions lies with the user and not with the website operator (*us*).
Using the taxable income (deductions made by the user) as a basis ensures that the calculated tax is correct.

The calculations are made as follows:

$$ CantonTax = (TaxableIncome * 0.0365) * (taxRateInPercent / 100) $$

$$ MunicipalityTax = (TaxableIncome * 0.0365) * (taxRateInPercent / 100) $$

$$ Tax = CantonTax + MunicipalityTax + (ChurchTax) $$

#### Example
> Taxable income = 70'000
>
> Simple Tax = 3.65% (=Steuerfuss) of taxable income --> 70000 * 3.65 / 100 = 2555
>
> Canton Tax = 2555 * (tax rate in percent) / 100
>
> Municipality Tax = 2555 * (tax rate in percent) / 100
>
> Tax = Canton Tax + Municipality Tax + (Church Tax)

#### Questions
- With or without church tax?
